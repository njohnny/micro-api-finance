package demo.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import demo.ApplicationFinancial;
import demo.model.PaymentOrderDTO;
import demo.service.PaymentService;

@Component
public class PaymentConsumer {

	@Autowired
	private PaymentService paymentService;
	
		private static final Logger LOG = LoggerFactory.getLogger(ApplicationFinancial.class);


		  @KafkaListener(topics = "${app.kafka.consumer.topics}", autoStartup = "${app.kafka.consumer.enabled}",
				  properties= {"spring.json.value.default.type=demo.model.PaymentOrderDTO"})
		  public void onMessage(ConsumerRecord<String, PaymentOrderDTO> record) {
			  
			  PaymentOrderDTO orderPaymentDTO = new PaymentOrderDTO();
			  
				orderPaymentDTO = record.value();
		    
			  LOG.info("FINANCIAL: CONSUMER-[order-SUBMITTED] - Received record [{}], topic: {}, partition: {}, offset: {}",  record.value(), record.topic(), record.partition(), record.offset());
			paymentService.processPayment(orderPaymentDTO);
			
			LOG.info("Passou no processamento de pagamento !");
		  }
	

}
