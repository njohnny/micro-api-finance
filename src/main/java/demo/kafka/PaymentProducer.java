package demo.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import demo.ApplicationFinancial;
import demo.model.PaymentOrderDTO;

@Component
public class PaymentProducer  {

	private static final Logger LOG = LoggerFactory.getLogger(ApplicationFinancial.class);


	@Autowired
	private KafkaTemplate<String, Object> templateProducer;

	public void send(String topic,PaymentOrderDTO payment) {

		templateProducer.send(topic, payment);

		LOG.info("FINANCIAL: Sent method Finalized [{}] to topic [{}]", payment, topic);
	}



}
